from django.conf.urls import url

from api.views import UserCreateView, UserDetail

urlpatterns = [
    url(r'restore', UserDetail.as_view(), name='user-detail'),
    url(r'^', UserCreateView.as_view(), name='users'),
]
