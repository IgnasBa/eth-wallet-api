from django.shortcuts import reverse

from rest_framework.test import APITestCase
from api.models import User


class TestUserApi(APITestCase):
    def setUp(self):
        self.user = User(user_name='stasys@sjdaasa.com', password='pass456', priv_key='oahdw021e')
        self.user.save()

    def test_user_creation(self):
        response = self.client.post(reverse('users'), {
            'user_name': 'stinta@igeaid.com',
            'password': 'pass789',
            'priv_key': '9doa2q9j',
        })

        self.assertEqual(User.objects.count(), 2)

        self.assertEqual(201, response.status_code)

