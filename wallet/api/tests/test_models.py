from django.test import TestCase

from api.models import User


class TestUserDataModel(TestCase):
    def setUp(self):
        self.user = User(user_name='saulys@sadsdsad.com', password="pass123", priv_key='wbn89wsh2')
        self.user.save()

    def test_user_creation(self):
        self.assertEqual(User.objects.count(), 1)
