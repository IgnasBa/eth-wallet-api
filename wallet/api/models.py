from django.db import models


class User(models.Model):
    id = models.CharField(max_length=64, primary_key=True)
    date_created = models.DateTimeField(auto_now_add=True)
    password = models.CharField(max_length=64)
    wallet_file = models.CharField(max_length=500)

    def __str__(self):
        return self.id
