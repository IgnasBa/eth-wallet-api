from rest_framework.serializers import ModelSerializer, Serializer, CharField
from api.models import User


class UserSerializer(ModelSerializer):
    class Meta:
        model = User
        fields = ('id', 'password', 'wallet_file', 'date_created')
        extra_kwargs = {
            'date_created': {'read_only': True},
        }


class BackupSerializer(ModelSerializer):
    class Meta:
        model = User
        fields = ('id', 'wallet_file', 'date_created')
        extra_kwargs = {
            'id': {'read_only': True},
            'date_created': {'read_only': True},
            'wallet_file': {'read_only': True},
        }


class RestoreRequestSerializer(Serializer):
        id = CharField(max_length=64)
        password = CharField(max_length=64)
