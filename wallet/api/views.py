from rest_framework import status
from rest_framework.response import Response
from rest_framework.generics import ListCreateAPIView, CreateAPIView
from api.models import User
from api.serializers import UserSerializer, BackupSerializer, RestoreRequestSerializer


class UserCreateView(ListCreateAPIView):
    queryset = User.objects.all()
    serializer_class = UserSerializer


class UserDetail(CreateAPIView):
    serializer_class = BackupSerializer

    def post(self, request, *args, **kwargs):
        try:
            if RestoreRequestSerializer(data=request.data).is_valid() is False:
                return Response({'message': 'Request does not have required fields'}, status=status.HTTP_400_BAD_REQUEST)

            user = User.objects.get(id=request.data['id'])
            valid_password = user.password == request.data['password']
            if valid_password:
                serialized = BackupSerializer(user)
                return Response(data=serialized.data, status=status.HTTP_200_OK)
            else:
                return Response({'message': 'Wrong pass'}, status=status.HTTP_401_UNAUTHORIZED)
        except User.DoesNotExist:
            return Response({'message': 'User do not exist'}, status=status.HTTP_404_NOT_FOUND)
